# REHeaRSE API

## Descrição

_API_ criada com o intuito de gerenciar a interação entre profissionais de saúde e pacientes.

Para consulta de definições da API e suas rotas, a aplicação provê através da Swagger UI uma breve documentação
na rota GET /api/v0/docs. Para obtenção da OpenAPI em formato JSON, consultar GET /api/v0/docs-json.

## Dependências

A aplicação é feita com as seguintes dependências:

- [NodeJS](https://nodejs.org/) ^v12.9
- [NestJS](https://docs.nestjs.com/) ^v7.5.1
- [PostgreSQL](https://www.postgresql.org/) ^v13.1
- [Passport](http://www.passportjs.org/docs/downloads/html/) ^v0.4.1

## Configuração

### Variáveis de Ambiente

|Nome                          |Descrição                           | Obrigatório | Exemplo               |
|------------------------------|------------------------------------|:-----------:|-----------------------|
|`LOG_LEVEL`                   | Nível de log                       |     ✗       |`INFO`                 |
|`PORT`                        | Porta a ser escutada pelo servidor |     ✗       |`3000`                 |
|`DB_HOST`                     | Hostname do banco                  |     ✔       |`localhost`            |
|`DB_PORT`                     | Porta do banco                     |     ✔       |`3000`                 |
|`DB_USER`                     | Credencial de usuário do banco     |     ✔       |`root`                 |
|`DB_SECRET`                   | Senha do banco                     |     ✔       |`rootpsw`              |
|`DB_NAME`                     | Nome do banco                      |     ✔       |`rehearse_api`         |
|`TOKEN_ISSUER`                | Nome do emissor do token           |     ✔       |`rehearse_api`         |
|`TOKEN_EXPIRATION_S`          | Expiração em segundos do token     |     ✔       |`60`                   |
|`TOKEN_SECRET`                | Segredo de criptografia do token   |     ✔       |`fa5sgtqag4as87`       |

## Como Desenvolver

### Localmente
- Instalar [PostgreSQL](https://www.postgresql.org/) ^v13.1
- Conectar no Sql Shell (instalado junto ao PostgreSQL)
- Executar no Sql Shell o comando:
```sql
CREATE DATABASE rehearse_api;
```
- Alterar as variáveis de ambiente de conexão com o banco de dados no arquivo .env, de acordo com a configuração feita na instalação do PostgreSQL.
- Acessar o diretório /api para instalar os pacotes utilizados na api e iniciar a aplicação.

#### Instalação de Dependências

```bash
$ npm install
```

#### Rodando a Aplicação

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

#### Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

### Docker

A aplicação é [distribuída](#como-utilizar) via containers Docker, e pode ser desenvolvida na mesma plataforma
preferencialmente através do [Docker-Compose](https://docs.docker.com/compose/reference/overview/) com o uso de .env
para configuração de variáveis de ambiente.

A porta 9229 é exposta para fins de debug.

#### Rodando a Aplicação

Primeiramente, deve-se estar na pasta rehearse/api e que se execute o seguinte comando:

```bash
$ npm install
```
Após feito isso, deve-se executar o seguinte comando:

```bash
$ docker-compose up
```


## Como Utilizar

A aplicação é distribuída via Docker e possui especificação OpenAPI 3 disponibilizada em duas rotas, posteriormente
citadas

### Construção Imagem Docker

```bash
# A partir da raíz (/api)
$ docker build --tag rehearse_api:latest .
```

### Executar Imagem Gerada

Com a imagem gerada pelo passo anterior

```bash
# Com arquivo variáveis em arquivo .env
$ docker run -p 3030:3030 --env-file <path_to_env> rehearse_api:latest

# Com variáveis configuradas na linha de comando
$ docker run -po 3030:3030 -e LOG_LEVEL=INFO \
                           -e PORT=3030
```

Obs.: Caso queira rodar a imagem de produção localmente, adicionar a opção ao comando --network api_api_net para integrar com a rede gerada pelo docker compose para conexão com o banco. 
O comando ficaria dessa maneira:

```bash
$ docker run -p 3030:3030 --network api_api_net --env-file .env rehearse_api:latest
```

###

## License

REHeaRSE API não possui licença([NO LICENSE](https://choosealicense.com/no-permission/)), portanto é de uso privado e
restrito, sob autorização.

